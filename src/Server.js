import express from 'express';

import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import Issue from './Model/Issue'

let app = express();
mongoose.connect('mongodb://localhost/test');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

let port = process.env.PORT || 8080;

let router = express.Router();

router.get('/', (req, res) => {
    res.json({message: "api testing"});
});

router
    .route('/Issue')
    .get((req, res) => {
        Issue.find((err, issues) => {

            if (err) 
                res.send(err);
            
            res.json(issues)

        })
    })
    .post((req, res) => {
        console.log(req.body);

        let issue = new Issue();
        issue.title = req.body.title;
        issue.priority = req.body.priority;
        issue.status = req.body.status;
        issue.owner = req.body.owner;

        issue.save((err) => {
            //console.log(err);
            if (err) {
                res.json(err)
            } else {
                res.json({message: "issues created"})
            }
        });

    });

router.use((req, res, next) => {
    console.log("middleware ");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/api', router);

app.listen(port);

//console.log('port ' + port);
