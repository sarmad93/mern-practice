import React, {Component} from 'react';
import {TableRowColumn, TableRow} from 'material-ui/Table';

export default class RenderTableRow extends Component {

    render() {

        return (
            <TableRow>
                <TableRowColumn>
                    {this.props.issue._id}
                </TableRowColumn>
                <TableRowColumn>
                    {this.props.issue.status}
                </TableRowColumn>
                <TableRowColumn>
                    {this.props.issue.priority}
                </TableRowColumn>
                <TableRowColumn>
                    {this.props.issue.owner}
                </TableRowColumn>
                <TableRowColumn>
                    {this.props.issue.title}
                </TableRowColumn>
            </TableRow>

        )

    }

}