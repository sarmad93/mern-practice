import React, {Component} from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import RenderTableRow from './RenderTableRow';

export default class IssueTable extends Component {

    render() {

        return (
            <Table>
                <TableHeader>
                    <TableRow>
                        <TableHeaderColumn>ID</TableHeaderColumn>
                        <TableHeaderColumn>Status</TableHeaderColumn>
                        <TableHeaderColumn>Priority</TableHeaderColumn>
                        <TableHeaderColumn>Owner</TableHeaderColumn>
                        <TableHeaderColumn>Title</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    {this
                        .props
                        .issues
                        .map((data, index) => {

                            return (<RenderTableRow key={index} issue={data}/>)
                        })}
                </TableBody>
            </Table>

        )
    }
}

/*
 {this
                        .props
                        .issues
                        .map((data, index) => {

                            return (<RenderTableRow key={index} issue={data}/>)
                        })}


 */