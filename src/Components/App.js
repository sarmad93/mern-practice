import React, {Component} from 'react';
import logo from '../logo.svg';
import '../App.css';
import OpenBug from './OpenBug.js';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IssueTable from './IssueTable'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      issues: []
    }
  }

  componentDidMount() {

    const issues = fetch("http://localhost:8080/api/Issue").then((res) => {
      res
        .json()
        .then((data) => {
          this.setState({issues: data});
        });
    }).catch((err) => {
      //  console.log("test", err);
    });
    /* console.log("test", issues);
     this.setState({
       issues
     });*/
  }

  render() {

    console.log("state " + this.state);
    return (

      <MuiThemeProvider muiTheme={getMuiTheme()}>

        <div className="App">

          <div className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
            <h2>Welcome to React</h2>
          </div>
          <div className="App-intro">
            <IssueTable issues={this.state.issues}/>
          </div>

        </div>

      </MuiThemeProvider>

    );
  }
}

export default App;
