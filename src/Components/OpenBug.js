import React, {Component} from 'react';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class OpenBug extends Component {

    constructor(props) {
        super(props);

        this.state = {
            priorityKey: 1,
            statusKey: 1,

            title: String,
            priority: String,
            status: String,
            owner: String
        }
        this.onPrioritySelected = this
            .onPrioritySelected
            .bind(this);
        this.onStatusSelected = this
            .onStatusSelected
            .bind(this);
        this.createIssue = this
            .createIssue
            .bind(this);
        this.onOwnerChange = this
            .onOwnerChange
            .bind(this);
        this.onTitleChange = this
            .onTitleChange
            .bind(this);

    }

    onPrioritySelected(ev, index, value) {
        console.log(value)
        this.setState({priorityKey: value, priority: value})
    }

    onStatusSelected(ev, index, value) {
        //this.state.status = value;
        this.setState({statusKey: value, status: value})
    }

    onTitleChange(ev, value) {

        this.setState({title: value})
        // console.log(value);

    }
    onOwnerChange(ev, value) {
        //console.log(value);

        this.setState({owner: value})
    }

    createIssue() {
        // this.state.title = console.log(value)

        fetch('http://localhost:8080/api/Issue', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({title: this.state.title, priority: this.state.priority, status: this.state.status, owner: this.state.owner})
        }).then((data) => {
            console.log('Request success: ', data);
        }).catch((error) => {
            console.log('Request failure: ', error);
        });
    }

    render() {

        return (
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <div>
                    <AppBar title="Open A Bug"/>

                    <TextField floatingLabelText="Title" onChange={this.onTitleChange}/>
                    <br/>

                    <SelectField
                        floatingLabelText="Priority"
                        value={this.state.priorityKey}
                        onChange={this.onPrioritySelected}>

                        <MenuItem value={"High"} primaryText="High"/>
                        <MenuItem value={"Low"} primaryText="Low"/>
                        <MenuItem value={"Critical"} primaryText="Critical"/>
                    </SelectField>
                    <br/>

                    <SelectField
                        floatingLabelText="Status"
                        value={this.state.statusKey}
                        onChange={this.onStatusSelected}>

                        <MenuItem value={"New"} primaryText="New"/>
                        <MenuItem value={"Open"} primaryText="Open"/>
                        <MenuItem value={"Closed"} primaryText="Closed"/>
                    </SelectField>
                    <br/>

                    <TextField floatingLabelText="Owner" onChange={this.onOwnerChange}/>
                    <br/>
                    <FlatButton label="Create Issue" onClick={this.createIssue}/>
                </div>

            </MuiThemeProvider>
        )
    }
}