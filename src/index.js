import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {BrowserRouter as Router, Route} from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from './Components/App';
import OpenBug from './Components/OpenBug.js';

injectTapEventPlugin();

ReactDOM.render((
  <Router>

    <div>
      <Route path="/App" component={App}/>
      <Route path="/OpenBug" component={OpenBug}/>
    </div>

  </Router>
), document.getElementById('root'));
