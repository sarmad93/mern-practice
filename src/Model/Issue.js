import mongoose from 'mongoose';

let schema= mongoose.Schema;


let issueSchema = new schema({
    title: String,
    priority : String,
    status : String,
    owner : String
});


module.exports= mongoose.model('Issue', issueSchema);